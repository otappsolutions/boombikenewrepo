﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AudioManager : MonoBehaviour
{

	public AudioSource characterSound, mainMusic;

	public AudioClip crashSfx,levelCompleteSfx,explosion;

	public static AudioManager Instance { get { return instance; } }

	private static AudioManager instance;


	void Awake ()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		} 
        
	}

	public void PlayExplosion ()
	{
		characterSound.PlayOneShot (explosion, 1.0f);
	}



	public void PlayMainMusic ()
	{
		mainMusic.Stop ();
		//mainMusic.clip = mainMusicMfx;
		mainMusic.Play ();
	}

	public void PlayHomeMfx ()
	{
		mainMusic.Stop ();
		//mainMusic.clip = homeMfx;
		mainMusic.Play ();
	}

	public void PlayBossMfx ()
	{
		mainMusic.Stop ();
		//mainMusic.clip = homeMfx;
		mainMusic.Play ();
	}
		



}
