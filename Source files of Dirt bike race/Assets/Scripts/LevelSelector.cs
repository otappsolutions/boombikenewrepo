﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelector : MonoBehaviour
{

	public int maxPage, currentPage;

	public GameObject lockItem1, lockItem2, textItem1, textItem2, bgItem1, bgItem2;

	public Sprite activeBg, deactiveBg;

	public GameObject loadingObj;

	public int currentLevel;

	// Use this for initialization
	void Start ()
	{
		PlayerPrefs.SetInt ("Level_1", 1);
		currentPage = 0;
		UpdatePage ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void UpdatePage ()
	{
		int lock1 = PlayerPrefs.GetInt ("Level_" + (currentPage * 2 + 1).ToString ());
		int lock2 = PlayerPrefs.GetInt ("Level_" + (currentPage * 2 + 2).ToString ());

		textItem1.GetComponent<Text> ().text = (currentPage * 2 + 1).ToString ();
		textItem2.GetComponent<Text> ().text = (currentPage * 2 + 2).ToString ();

		if (lock1 == 1) {

			lockItem1.SetActive (false);
			bgItem1.GetComponent<Image> ().sprite = activeBg;
		} else {

			lockItem1.SetActive (true);
			bgItem1.GetComponent<Image> ().sprite = deactiveBg;
		}


		if (lock2 == 1) {

			lockItem2.SetActive (false);
			bgItem2.GetComponent<Image> ().sprite = activeBg;
		} else {

			lockItem2.SetActive (true);
			bgItem2.GetComponent<Image> ().sprite = deactiveBg;
		}
	}


	public void NextPage ()
	{
		if (currentPage <= maxPage - 1) {
			currentPage++;
			UpdatePage ();
		}
	}

	public void PrePage ()
	{
		if (currentPage > 0) {
			currentPage--;
			UpdatePage ();
		}
	}

	public void OpenLevel (int _value)
	{
		int _lock = PlayerPrefs.GetInt ("Level_" + (currentPage * 2 + _value).ToString ());
		currentLevel = currentPage * 2 + _value;
		if (_lock == 1) {

			StartCoroutine (StartLevel ());
		}
	}

	IEnumerator StartLevel ()
	{
		loadingObj.SetActive (true);
		yield return new WaitForSeconds (2.0f);
		Application.LoadLevel (currentLevel);
	}
}
