﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorController : MonoBehaviour
{


	public int maxMotorSpeed = 1500;
	//if we use backtire motor we use this
	public int maxBackTireTorque = -1500;
	public int speedStepPlus = 300;
	//if we use backtire motor we use this
	public float BackTireTorqueStep = -50f;
	public int speedStepMinus = 100;
	public int brakeStep = 10;
	public float torqgueForce = 4000;
	public HingeJoint2D backTire, frontTire;
	public Rigidbody2D bikeBody;
	public CheckGround groundChecker;
	public float jumpForce;

	//private variables
	private bool accelerateOn = false;
	private bool brakeOn = false;
	private bool leanBackOn = false;
	private bool leanForwardOn = false;
	private bool playerStarted = false;
	private bool crashed = false;

	//bike control states
	private bool leanBack = false;
	private bool leanForward = false;
	private bool brake = false;
	private bool accelerate = false;

	public AudioSource EngineSoundS;


	// Use this for initialization
	void Start ()
	{


	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	void FixedUpdate ()
	{

		if (!crashed) {

			accelerateFunction2 ();
			brakeFunction ();
			leanBackFunction ();
			leanForwardFunction ();
			EngineSoundMobile ();

		}

	}

	//this function uses torque to accelerate
	void accelerateFunction2 ()
	{


		if (accelerate && !brakeOn && groundChecker.isOnGroundCheck ()) {
			callStartTimer ();

			if (backTire.GetComponent<Rigidbody2D> ().angularVelocity > maxBackTireTorque) {

				backTire.GetComponent<Rigidbody2D> ().AddTorque (BackTireTorqueStep);

			}

		} else { //deaccelerate

			accelerateOn = false;

			backTire.useMotor = false;
		}
	}

	void callStartTimer ()
	{

		if (!playerStarted) {
			playerStarted = true;
		}

	}

	//this accelerate function is based on motor functionality

	void brakeFunction ()
	{

		if (brake && groundChecker.isOnGroundCheck ()) {	
			backTire.useMotor = false;
			brakeOn = true;
			bikeBody.drag = bikeBody.drag + (brakeStep * Time.deltaTime);
		} else {
			brakeOn = false;
			bikeBody.drag = 0;
		}
	}

	void leanBackFunction ()
	{

		if (leanBack) {
			leanBackOn = true;
			bikeBody.AddTorque (torqgueForce);
		} else {
			leanBackOn = false;
		}
	}

	void leanForwardFunction ()
	{

		if (leanForward) {
			leanForwardOn = true;
			bikeBody.AddTorque (-torqgueForce);

		} else {
			leanForwardOn = false;
		}
	}



	public void setCrashed (bool par)
	{
		crashed = par;
	}

	public void Jump ()
	{
		backTire.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, jumpForce));
	}

	public void DoBack ()
	{
		leanBack = true;
	}

	public void StopBack ()
	{
		leanBack = false;
	}

	public void DoForward ()
	{
		leanForward = true;
	}

	public void StopForward ()
	{
		leanForward = false;
	}

	public void DoAccelerate ()
	{
		accelerate = true;
	}

	public void StopAccelerate ()
	{
		accelerate = false;
	}

	public void DoBrake ()
	{
		brake = true;
	}

	public void StopBrake ()
	{
		brake = false;
	}

	public void EngineSoundMobile ()
	{
		
		if (bikeBody.velocity.x < 10.0f) {
			EngineSoundS.pitch = 1.0f;
		} else {
			EngineSoundS.pitch = Mathf.Lerp (EngineSoundS.pitch, Mathf.Clamp (1.5f, 1.0f, 2.4f), Time.deltaTime * 1.5f);
		}
	}
}
