﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

	public GameObject pausePanel, crashPanel, clearPanel;

	public string moreGame;

	public Text levelText;

	public Transform startPos, targetPos, bikePos;

	public RectTransform bikePosUI;

	// Use this for initialization
	void Start ()
	{
		levelText.text = "Level " + Application.loadedLevel.ToString ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if ((bikePos.position.x >= startPos.position.x)
		   && (bikePos.position.x <= targetPos.position.x))
			bikePosUI.localPosition = new Vector3 ((bikePos.position.x - startPos.position.x) * 500.0f / (targetPos.position.x - startPos.position.x) - 250.0f,
				bikePosUI.localPosition.y, bikePosUI.localPosition.z);
	}

	public void Pause ()
	{
		Time.timeScale = 0.0f;
		pausePanel.SetActive (true);
		AdsControl.Instance.showAds ();
	}

	public void Resume ()
	{
		Time.timeScale = 1.0f;
		pausePanel.SetActive (false);
	}

	public void ShowCrash ()
	{
		StartCoroutine (IEShowCrash ());
	}

	public void ShowClear ()
	{
		StartCoroutine (IEShowClear ());
	}

	IEnumerator IEShowCrash ()
	{
        Debug.Log("Made it to crash");
        yield return new WaitForSeconds (2.0f);
        AdsControl.Instance.showAds ();
        FireBaseManager.instance.LogScreen ("CRASH LEVEL " + Application.loadedLevel);
        crashPanel.SetActive(true);
        Time.timeScale = 0.0f;
	}

	IEnumerator IEShowClear ()
	{
		yield return new WaitForSeconds (2.0f);
		AdsControl.Instance.showAds ();
		PlayerPrefs.SetInt ("Level_" + (Application.loadedLevel + 1).ToString (), 1);
		FireBaseManager.instance.LogScreen ("COMPLETE LEVEL " + Application.loadedLevel);
		clearPanel.SetActive (true);
		Time.timeScale = 0.0f;
	}

	public void NextLevel ()
	{
		Time.timeScale = 1.0f;
		Application.LoadLevel (Application.loadedLevel + 1);
	}

	public void RePlay ()
	{
		Time.timeScale = 1.0f;
		Application.LoadLevel (Application.loadedLevel);
	}

	public void Back ()
	{
		Time.timeScale = 1.0f;
		Application.LoadLevel (0);
	}

	public void MoreGame ()
	{
		Time.timeScale = 1.0f;
		Application.OpenURL (moreGame);
		FireBaseManager.instance.LogScreen ("MORE GAME IN GAME");
	}
}
