﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

	//broken bike
	public GameObject ExplosionObj;

	public GameObject BikeBroken;

	public GameObject Bike;

	public GameObject Player;

	public HingeJoint2D head;

	public HingeJoint2D body;

	public HingeJoint2D arm1;

	public HingeJoint2D arm2;

	public HingeJoint2D arm3;

	public HingeJoint2D leg1;

	public HingeJoint2D leg2;


	public static GameManager _gameManager;

	public bool isGameOver;

	public Transform startPoint;

	public UIManager _UIManager;


	void Awake ()
	{
		_gameManager = this;
		Bike.transform.position = startPoint.position;
		FireBaseManager.instance.LogScreen ("LEVEL_" + Application.loadedLevel);
		Application.targetFrameRate = 60;
	}

	// Use this for initialization
	void Start ()
	{
		isGameOver = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void BrokenBike (Transform _transform)
	{
		if (!isGameOver) {
			isGameOver = true;
			Instantiate (ExplosionObj, _transform.parent.position, _transform.parent.rotation);
			Instantiate (BikeBroken, _transform.parent.position, _transform.parent.rotation);

			Player.transform.parent = null;

			head.enabled = false;
			body.enabled = false;
			arm1.enabled = false;
			arm2.enabled = false;
			arm3.enabled = false;
			leg1.enabled = false;
			leg2.enabled = false;

			head.GetComponent<Collider2D> ().isTrigger = false;
			body.GetComponent<Collider2D> ().isTrigger = false;
			arm1.GetComponent<Collider2D> ().isTrigger = false;
			arm2.GetComponent<Collider2D> ().isTrigger = false;
			arm3.GetComponent<Collider2D> ().isTrigger = false;
			leg1.GetComponent<Collider2D> ().isTrigger = false;
			leg2.GetComponent<Collider2D> ().isTrigger = false;

			//Destroy (Bike);
			Bike.SetActive (false);
			_UIManager.ShowCrash ();
			if (AudioManager.Instance != null)
				AudioManager.Instance.PlayExplosion ();
		}
	}


	public void Finish ()
	{
		CameraController._camera.followPlayer = false;
		_UIManager.ShowClear ();
		Bike.GetComponent<AudioSource> ().mute = true;
	}
}
