﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorInput : MonoBehaviour {

	public MotorController _motor;
	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {


		if (Input.GetKeyDown (KeyCode.RightArrow))
			_motor.DoAccelerate ();
		if (Input.GetKeyUp (KeyCode.RightArrow))
			_motor.StopAccelerate ();

		if (Input.GetKeyDown (KeyCode.LeftArrow))
			_motor.DoBrake ();
		if (Input.GetKeyUp (KeyCode.LeftArrow))
			_motor.StopBrake ();

		if (Input.GetKeyDown (KeyCode.DownArrow))
			_motor.DoForward ();
		if (Input.GetKeyUp (KeyCode.DownArrow))
			_motor.StopForward ();

		if (Input.GetKeyDown (KeyCode.UpArrow))
			_motor.DoBack ();
		if (Input.GetKeyUp (KeyCode.UpArrow))
			_motor.StopBack ();
		if (Input.GetKeyDown (KeyCode.Space))
			_motor.Jump ();
	}

	public void Jump()
	{
		_motor.Jump ();
	}

	public void DoAccelerate ()
	{
		_motor.DoAccelerate ();
	}

	public void StopAccelerate ()
	{
		_motor.StopAccelerate ();
	}

	public void DoBrake ()
	{
		_motor.DoBrake ();
	}

	public void StopBrake ()
	{
		_motor.StopBrake ();
	}

	public void DoForWard ()
	{
		_motor.DoForward ();
	}

	public void StopForWard ()
	{
		_motor.StopForward ();
	}

	public void DoBack ()
	{
		_motor.DoBack ();
	}

	public void StopBack()
	{
		_motor.StopBack ();
	}
}
