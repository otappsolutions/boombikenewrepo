﻿using UnityEngine;
using System.Collections;

public class CheckGround : MonoBehaviour {

	public bool isOnGround = false;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other) {

		if(other.gameObject.tag == "ground" || other.gameObject.tag == "loop" || other.gameObject.tag == "box"
			|| other.gameObject.tag == "chain") {
			isOnGround = true;
		}
	}

	void OnTriggerExit2D(Collider2D other) {

		if(other.gameObject.tag == "ground" || other.gameObject.tag == "loop" || other.gameObject.tag == "box" 
			|| other.gameObject.tag == "chain") {
			isOnGround = false;
		}

	}

	void OnTriggerStay2D(Collider2D other) {

		if(other.gameObject.tag == "ground" || other.gameObject.tag == "loop" || other.gameObject.tag == "box" 
			|| other.gameObject.tag == "chain") {
			isOnGround = true;
		}

	}

	void OnCollisionEnter2D(Collision2D other) {

		if(other.gameObject.tag == "ground" || other.gameObject.tag == "loop" || other.gameObject.tag == "box"
			|| other.gameObject.tag == "chain") {
			isOnGround = true;
		}
	}

	void OnCollisionStay2D(Collision2D other) {

		if(other.gameObject.tag == "ground" || other.gameObject.tag == "loop" || other.gameObject.tag == "box"
			|| other.gameObject.tag == "chain") {
			isOnGround = true;
		}
	}

	void OnCollisionExit2D(Collision2D other) {

		if(other.gameObject.tag == "ground" || other.gameObject.tag == "loop" || other.gameObject.tag == "box"
			|| other.gameObject.tag == "chain") {
			isOnGround = false;
		}

	}

	public bool isOnGroundCheck () {
		return isOnGround;
	}


}
