﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeManager : MonoBehaviour
{
	public GameObject levelPanel;

	public string urlLink;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void ShowLevelPanel ()
	{
		levelPanel.SetActive (true);
	}

	public void BackToHome ()
	{
		levelPanel.SetActive (false);
	}

	public void MoreGame ()
	{
		FireBaseManager.instance.LogScreen ("MORE GAME IN HOME");
		Application.OpenURL (urlLink);
	}


}
