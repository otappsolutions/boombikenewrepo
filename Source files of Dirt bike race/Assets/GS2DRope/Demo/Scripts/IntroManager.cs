﻿using UnityEngine;

public class IntroManager : MonoBehaviour {
    
    public void LoadScene(int sceneBuildIndex)
    {
        Application.LoadLevel(sceneBuildIndex);
    }

    public void OpenAssetStorePage()
    {
        Application.OpenURL("https://www.assetstore.unity3d.com/en/#!/publisher/10488/");
    }
}
