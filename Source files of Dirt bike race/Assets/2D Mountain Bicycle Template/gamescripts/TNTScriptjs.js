﻿#pragma strict

	 var radius = 200.0;
	 var power = 900000.0;
	 var myCollider : BoxCollider2D;
	 var animat : Animator;
	 //private variables
	 private var exploded : boolean = false;
	function Start () {

	}
	
	

function Update () {

}

function OnCollisionEnter2D(coll: Collision2D) {

	if (coll.gameObject.tag == "tire" && !exploded) {
		doAction();
	}
		
}

function explode() {

		// Applies an explosion force to all nearby rigidbodies
		var explosionPos : Vector2 = transform.position;
		var colliders : Collider2D[] = Physics2D.OverlapCircleAll(explosionPos, radius);
		
		for (var hit : Collider2D in colliders) {
			if (hit.tag != "tnt") { //hit.tag == "box" || )
				Debug.Log("box hit");
				/*
				var dir = (hit.transform.position - explosionPos);
        		var wearoff = 1 - (dir.magnitude / radius);
        		if(hit.rigidbody2D != null) {
       	    		hit.rigidbody2D.AddForce(dir.normalized * power * wearoff);
       	    	}
       	    	*/
       	    	
       	    	if(hit.GetComponent.<Rigidbody2D>() != null) {
       	    	 var dir = (hit.transform.position - explosionPos);
        	     var wearoff = 1 - (dir.magnitude / radius);
       			 var baseForce = dir.normalized * power * wearoff;
        		 hit.GetComponent.<Rigidbody2D>().AddForce(baseForce);
 
        	     var upliftWearoff = 1 - 3 / radius;
        		 var upliftForce = Vector2.up * power * upliftWearoff;
        	     hit.GetComponent.<Rigidbody2D>().AddForce(upliftForce);
        	     
        	     //rotatin force
        	     hit.GetComponent.<Rigidbody2D>().AddTorque(100);
        	    }
       	    }
        
		}
		
}

function destroyTNT() {
	//this will be called by the animation clip
	Destroy(this.gameObject);
}

function doAction() {
	    exploded = true;
	    myCollider.isTrigger = true;
		explode();
		animat.SetBool("exploded",true);
}
