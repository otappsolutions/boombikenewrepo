﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BikeController : MonoBehaviour {


	public int maxMotorSpeed = 1500; //if we use backtire motor we use this
	public int maxBackTireTorque = -1500;
	public int maxPiderSpeed = 400;
	public int speedStepPlus = 300; //if we use backtire motor we use this
	public float BackTireTorqueStep = -50f;
	public int speedStepMinus = 100;
	public int piderSpeedStepPlus = 800;
	public int brakeStep = 10;
	public float torqgueForce = 4000;
	public HingeJoint2D backTire;
	public HingeJoint2D pider;
	public Rigidbody2D bikeBody;
	public CheckForGround groundChecker;
	
	

	public GameObject bike1;
	public GameObject bike2;
	public GameObject bike3;
	
	public int myBikeNumber;
	
	public AudioClip freeSpinSound;
	public AudioClip ridingSound;
	public AudioClip fallingSound;

	//private variables
	private bool accelerateOn = false;
	private bool brakeOn = false;
	private bool leanBackOn = false;
	private bool leanForwardOn = false;
	private bool crashed = false;
	private bool playerStarted = false; 
	private SceneConrtoller sceneController;
	private tk2dTileMapDemoFollowCam tk2dFollow;
	private bool freeSpinSoundIsPlaying = false;
	private bool ridingSoundIsPlaying = false;
	private bool crashedSoundPlaying = false;
	private bool levelFinished = false;

	//bike control states
	private bool leanBack = false;
	private bool leanForward = false;
	private bool brake = false;
	private bool accelerate = false;
	

	void OnEnable(){
		
	}
	
	// Use this for initialization
	void Start () {
		Application.targetFrameRate = 300; //this helps remove the jitter http://docs.unity3d.com/ScriptReference/Application-targetFrameRate.html
		
		sceneController = GameObject.FindWithTag("gamemnu").GetComponent<SceneConrtoller>();
		tk2dFollow = GameObject.FindGameObjectWithTag("cameholder").GetComponent<tk2dTileMapDemoFollowCam>();
		tk2dFollow.target = gameObject.transform;
		showSelectedBike();

		
		InvokeRepeating("playSound",0f,0.2f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void FixedUpdate() {
		
		if(!crashed) {
			
			accelerateFunction2();
			brakeFunction();
			leanBackFunction();
			leanForwardFunction();

		}
		
	}
	

	//this function uses torque to accelerate
	void accelerateFunction2 () {
		
		JointMotor2D piderMotor = pider.motor;
		
		if(accelerate && !brakeOn && groundChecker.isOnGroundCheck()) {
			callStartTimer();

			if( backTire.GetComponent<Rigidbody2D>().angularVelocity > maxBackTireTorque) {
				
				backTire.GetComponent<Rigidbody2D>().AddTorque(BackTireTorqueStep);
				
			}
			
			//pider spin speed
			pider.useMotor = true;
			piderMotor.motorSpeed = pider.motor.motorSpeed + (Time.deltaTime * piderSpeedStepPlus);
			pider.motor = piderMotor;
			
			if(pider.motor.motorSpeed > maxPiderSpeed) {
				
				piderMotor.motorSpeed = maxPiderSpeed;
				pider.motor = piderMotor;
			}
			
		}
		else { //deaccelerate
			
			accelerateOn = false;
			
			//pider spin slow down
			piderMotor.motorSpeed = pider.motor.motorSpeed - (Time.deltaTime * speedStepMinus);
			pider.motor = piderMotor;
			
			if(pider.motor.motorSpeed < 0) {
				piderMotor.motorSpeed = 0;
				pider.motor = piderMotor;
			}
			
			backTire.useMotor = false;
			pider.useMotor = false;
		}

	}
	
	//this accelerate function is based on motor functionality

	
	void brakeFunction () {
		
		if(brake && groundChecker.isOnGroundCheck()) {	
			backTire.useMotor = false;
			brakeOn = true;
			bikeBody.drag = bikeBody.drag + (brakeStep * Time.deltaTime);
		}
		else {
			brakeOn = false;
			bikeBody.drag = 0;
		}
		
		
	}
	
	void leanBackFunction () {
		
		if(leanBack) {
			leanBackOn = true;
			bikeBody.AddTorque(torqgueForce);
		}
		else {
			leanBackOn = false;
		}
	}
	
	void leanForwardFunction () {
		
		if(leanForward) {
			leanForwardOn = true;
			bikeBody.AddTorque(-torqgueForce);
			
		}
		else {
			leanForwardOn = false;
		}
	}

	
	
	public void setCrashed(bool par) {
		crashed = par;
	}
	
	
	void OnTriggerEnter2D(Collider2D other) {
		
		if(other.gameObject.tag == "archplatform") {
			//transform.parent = other.gameObject.transform;
			Vector3 posi = transform.position;
			posi.x = other.gameObject.transform.position.x;
			transform.position = posi;
			Debug.Log("arch triggered");
		}
	}
	
	void OnCollisionEnter2D(Collision2D other) {
		
		if(other.gameObject.tag == "archplatform") {
			//transform.parent = other.gameObject.transform;
			Vector3 posi = transform.position;
			posi.x = other.gameObject.transform.position.x;
			transform.position = posi;
			Debug.Log("arch triggered");
		}
	}
	
	void callStartTimer () {
		
		if(!playerStarted) {
			
			sceneController.startTimer();
			playerStarted = true;
		}
		
	}
	
	void showSelectedBike () {
		
		int whichBike;
		GameObject obj = gameObject;
		BikeController controllerClone;
		
		if(PlayerPrefs.HasKey("selectedBike")) { //we are playing for the first time
			whichBike = PlayerPrefs.GetInt("selectedBike");
		}
		else {
			whichBike = 1;
		}
		
		if(myBikeNumber == whichBike) {
			return; //selected bike is already showing
		}
		
		
		switch(whichBike) {
			
		case 1:
			obj = (GameObject) Instantiate(bike1, gameObject.transform.position, Quaternion.identity);
			break;
		case 2:
			obj = (GameObject) Instantiate(bike2, gameObject.transform.position, Quaternion.identity);
			break;
		case 3:
			obj = (GameObject) Instantiate(bike3, gameObject.transform.position, Quaternion.identity);
			break;
		}
		
		//copy the model bike setting for the current level to the cloned bike (the bike the user choosed)
		controllerClone = obj.GetComponent<BikeController>();
		
		controllerClone.maxMotorSpeed = maxMotorSpeed; //if we use backtire motor we use this
		controllerClone.maxBackTireTorque = maxBackTireTorque;
		controllerClone.maxPiderSpeed = maxPiderSpeed;
		controllerClone.speedStepPlus = speedStepPlus; //if we use backtire motor we use this
		controllerClone.BackTireTorqueStep = BackTireTorqueStep;
		controllerClone.speedStepMinus = speedStepMinus;
		controllerClone.piderSpeedStepPlus = piderSpeedStepPlus;
		controllerClone.brakeStep = brakeStep;
		controllerClone.torqgueForce = torqgueForce;
		
		Destroy(gameObject);
	}
	
	void playSound() {
		
		
		if(crashedSoundPlaying || levelFinished) {
			return;
		}
		
		if(crashed && !crashedSoundPlaying) {
			GetComponent<AudioSource>().Stop();
			GetComponent<AudioSource>().volume = 10.0f;
			GetComponent<AudioSource>().PlayOneShot(fallingSound);
			crashedSoundPlaying = true;
			return;
		}
		
		
		if(!playerStarted) {
			return;
		}
		
		
		
		if(backTire.GetComponent<Rigidbody2D>().velocity.magnitude <= 10f) {
			GetComponent<AudioSource>().Stop();
			return;
		}
		
		if((backTire.GetComponent<Rigidbody2D>().velocity.magnitude >= 10f) && 
		   !groundChecker.isOnGroundCheck() && !freeSpinSoundIsPlaying) {
			GetComponent<AudioSource>().Stop();
			GetComponent<AudioSource>().clip = freeSpinSound;
			GetComponent<AudioSource>().Play();
			freeSpinSoundIsPlaying = true;
			ridingSoundIsPlaying = false;
			return;
		}
		
		if((backTire.GetComponent<Rigidbody2D>().velocity.magnitude >= 10f) && 
		   groundChecker.isOnGroundCheck() && !ridingSoundIsPlaying) {
			GetComponent<AudioSource>().Stop();
			GetComponent<AudioSource>().clip = ridingSound;
			GetComponent<AudioSource>().Play();
			freeSpinSoundIsPlaying = false;
			ridingSoundIsPlaying = true;
		}
		
	}
	
	public void stopSounds() {
		levelFinished = true;
		GetComponent<AudioSource>().Stop();
	}

	public void doLeanBack () {
		leanBack = true;
	}

	public void stopLeanBack() {
		leanBack = false;
	}

	public void doLeanForward () {
		leanForward = true;
	}
	
	public void stopLeanForward() {
		leanForward = false;
	}

	public void doAccelerate () {
		accelerate = true;
	}
	
	public void stopAccelerate() {
		accelerate = false;
	}

	public void doBrake () {
		brake = true;
	}
	
	public void stopBrake() {
		brake = false;
	}
}

