﻿using UnityEngine;
using System.Collections;

public class DisableLoopParts : MonoBehaviour {

	public BoxCollider2D[] loopClooidersDisable;
	public BoxCollider2D[] loopClooidersEnable;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {

		disableColliders();
	}


	void OnCollisionEnter2D(Collision2D other) {

		disableColliders();
	}

	void disableColliders() {
		foreach (BoxCollider2D clooider in loopClooidersDisable) {
			clooider.isTrigger = true;
		}

		foreach (BoxCollider2D clooider2 in loopClooidersEnable) {
			clooider2.isTrigger = false;
		}
	}
}
