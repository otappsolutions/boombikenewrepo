﻿using UnityEngine;
using System.Collections;

public class AnimationTrigger : MonoBehaviour {

	public MovingArchScript movingArchScript;
	public bool playInReverse = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {

		if(other.gameObject.tag == "player") {
			playAnimation();
		}
	}
	
	void OnCollisionEnter2D(Collision2D other) {

		if(other.gameObject.tag == "player") {
			playAnimation();
		}
	}

	void playAnimation() {
	
		if(!playInReverse) {
			
			if(movingArchScript != null) {
				movingArchScript.playAnimation();
			}
		}
		else {
			if(movingArchScript != null) {
				movingArchScript.playAnimationReverse();
			}
		}

	}
}
