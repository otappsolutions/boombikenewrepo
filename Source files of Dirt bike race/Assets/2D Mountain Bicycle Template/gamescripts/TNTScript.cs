﻿using UnityEngine;
using System.Collections;

public class TNTScript : MonoBehaviour {

	public float radius = 1000.0F;
	public float power = 100000.0F;
	void Start() {
		Vector2 explosionPos = transform.position;
		Debug.Log(explosionPos);
		Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPos, radius);
		foreach (Collider2D hit in colliders) {
			if (hit.tag == "box") {

				Debug.Log("boom");
				Vector2 par1 = hit.transform.position;
				Vector2 dir = (par1 - explosionPos);
				float wearoff = 1 - (dir.magnitude / radius);
				hit.GetComponent<Rigidbody2D>().AddForce(dir.normalized * power * wearoff);

			}
			
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
