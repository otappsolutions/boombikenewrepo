﻿#pragma strict

var tntscript : TNTScriptjs;
private var triggered : boolean = false;

function Start () {

}

function Update () {

}

function OnTriggerEnter2D(other: Collider2D) {
	
	if(other.gameObject.tag == "bikebody" && !triggered) {
	 	triggered = true;
		tntscript.doAction();
	}
}

function OnCollisionEnter2D(coll: Collision2D) {

	if (coll.gameObject.tag == "bikebody" && !triggered) {
		triggered = true;
		tntscript.doAction();
	}
		
}