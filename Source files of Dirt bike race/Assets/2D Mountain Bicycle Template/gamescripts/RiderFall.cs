﻿using UnityEngine;
using System.Collections;

public class RiderFall : MonoBehaviour {

	public BoxCollider2D[] bodyParts;
	public BoxCollider2D riderBodyCollider;
	public PolygonCollider2D bikeBodyCollider;
	public GameObject arm;
	public GameObject riderBody;
	public GameObject legFront;
	public GameObject legBack;
	public GameObject pider;
	public GameObject bikeBody;
	public BikeController bikeController;
	public tk2dTileMapDemoFollowCam cam;
	//private variables
	private SceneConrtoller sceneController;
	private bool fallen = false;
	// Use this for initialization
	void Start () {
		cam = GameObject.FindGameObjectWithTag("cameholder").GetComponent<tk2dTileMapDemoFollowCam>();
	}
	
	// Update is called once per frame
	void Update () {

	}


	void OnTriggerEnter2D(Collider2D other) {
		
		if(other.tag == "ground" || other.gameObject.tag == "box" ) { //|| other.gameObject.tag == "loop") {
			DestroyJoints(arm);
			DestroyJoints(legFront);
			DestroyJoints(legBack);
			DestroyJoints(riderBody);
			DestroyJoints(pider);
			DestroyJoints(bikeBody);
			bikeController.setCrashed(true);
			cam.target = this.gameObject.transform;
			riderBodyCollider.isTrigger = false;
			bikeBodyCollider.isTrigger = false;



			sceneController = GameObject.FindWithTag("gamemnu").GetComponent<SceneConrtoller>();
			sceneController.stopTimer();
			Invoke("callGameOver2",2);
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
		
		if(other.gameObject.tag == "ground" || other.gameObject.tag == "box" || other.gameObject.tag == "loop") {
			DestroyJoints(arm);
			DestroyJoints(legFront);
			DestroyJoints(legBack);
			DestroyJoints(riderBody);
			DestroyJoints(pider);
			DestroyJoints(bikeBody);
			bikeController.setCrashed(true);
			cam.target = this.gameObject.transform;
			riderBodyCollider.isTrigger = false;
			bikeBodyCollider.isTrigger = false;

			foreach (BoxCollider2D bodypart in bodyParts) {
				bodypart.isTrigger = false;
		}

			Invoke("callGameOver2",2);
	}
}


	void DestroyJoints(GameObject par) {
		HingeJoint2D[] hingeJoints;
		SpringJoint2D[] springJoints;

		hingeJoints = par.GetComponents<HingeJoint2D>();
		foreach (HingeJoint2D joint in hingeJoints) {

			if(joint.connectedBody.gameObject.tag == "bikebody" || joint.connectedBody.gameObject.tag == "pider"
			   || joint.connectedBody.gameObject.tag == "player") {

				//special for pider joint
				if(joint.tag == "pider" && joint.connectedBody.gameObject.tag == "bikebody" ||
				   joint.tag == "player" && joint.connectedBody.gameObject.tag == "player") {
					continue;
				}
				else {
					joint.enabled = false;	
				}
			}
		}


		springJoints = par.GetComponents<SpringJoint2D>();
		foreach (SpringJoint2D joint in springJoints) {
			
			if(joint.connectedBody.gameObject.tag == "bikebody" || joint.connectedBody.gameObject.tag == "pider"
			   || joint.connectedBody.gameObject.tag == "player") {
				joint.enabled = false;	
			}
		}
	}
	

	void callGameOver2() {

		if(fallen) 
			return; //show only once

		fallen = true;
		sceneController.showGameOver(fallen);
	}
}
