﻿using UnityEngine;
using System.Collections;

public class CheckerFlagScript : MonoBehaviour {

	public Animator animator;
	public tk2dTileMapDemoFollowCam cam;
	public int myLevel;
	private SceneConrtoller sceneController;
	private BikeController bikeController;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {

		if(other.gameObject.tag == "player") {
			bikeController = GameObject.FindGameObjectWithTag("bikebody").GetComponent<BikeController>();
			bikeController.stopSounds();
			sceneController = GameObject.FindWithTag("gamemnu").GetComponent<SceneConrtoller>();
			sceneController.stopTimer();
			swingFlag();
			GetComponent<AudioSource>().Play();
			Invoke("callGameOver",2);
			PlayerPrefs.SetInt(Application.loadedLevelName,1); //set this level as completed to unlock next level
		}
	}

	void OnCollisionEnter2D(Collision2D other) {

		if(other.gameObject.tag == "player") {
			swingFlag();
		}
	}

	void swingFlag() {

		animator.SetBool("touched",true);
		cam = GameObject.FindGameObjectWithTag("cameholder").GetComponent<tk2dTileMapDemoFollowCam>();
		cam.target = this.gameObject.transform;
	}


	void callGameOver() {

		sceneController.showGameOver(false); //player finished the level 
	}
}
