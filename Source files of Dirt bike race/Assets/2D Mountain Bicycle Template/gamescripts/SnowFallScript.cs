﻿using UnityEngine;
using System.Collections;

public class SnowFallScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "snow";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
