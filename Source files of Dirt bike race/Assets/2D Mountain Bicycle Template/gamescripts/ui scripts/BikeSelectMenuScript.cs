﻿using UnityEngine;
using System.Collections;

public class BikeSelectMenuScript : MonoBehaviour {

	public ScoreKeeperScript scoreKeeper;
	public GameObject bike1Selected;
	public GameObject bike2Selected;
	public GameObject bike3Selected;
	public GameObject bike2lock;
	public GameObject bike3lock;
	public GameObject bike2points;
	public GameObject bike3points;
	public RectTransform bikeSelectionPanel;

	private int unlockBike2Points = 5000;
	private int unlockBike3Points = 8000;
	private int totalScore;

	// Use this for initialization
	void Start () {

		totalScore = scoreKeeper.getTotalScore();

		if(totalScore >= unlockBike2Points) {
			bike2points.SetActive(false);
			bike2lock.SetActive(false);
		}

		if(totalScore >= unlockBike3Points) {
			bike3points.SetActive(false);
			bike3lock.SetActive(false);
		}

		//check which bike is selected

		if(PlayerPrefs.HasKey("selectedBike")) {

			Vector3 pos = bikeSelectionPanel.transform.localPosition;

			int whichBike = PlayerPrefs.GetInt("selectedBike");

			if(whichBike == 1) {
				bike1Selected.SetActive(true);
				bike2Selected.SetActive(false);
				bike3Selected.SetActive(false);
			}

			if(whichBike == 2) {
				bike1Selected.SetActive(false);
				bike2Selected.SetActive(true);
				bike3Selected.SetActive(false);
				pos.x = -100.2174f;
				bikeSelectionPanel.transform.localPosition = pos;
			}

			if(whichBike == 3) {
				bike1Selected.SetActive(false);
				bike2Selected.SetActive(false);
				bike3Selected.SetActive(true);
				pos.x = -234.5015f;
				bikeSelectionPanel.transform.localPosition = pos;
			}
		}
		else { //the variable not even set so means we are playing for the first time
			//default bike selected is bike 1 then
			PlayerPrefs.SetInt("selectedBike",1);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void selectBike1() {
	
		PlayerPrefs.SetInt("selectedBike",1);

		bike1Selected.SetActive(true);
		bike2Selected.SetActive(false);
		bike3Selected.SetActive(false);
	}

	public void selectBike2() {

		if(totalScore >= unlockBike2Points) {
			PlayerPrefs.SetInt("selectedBike",2);
			bike1Selected.SetActive(false);
			bike2Selected.SetActive(true);
			bike3Selected.SetActive(false);
		}
	}

	public void selectBike3() {
		
		if(totalScore >= unlockBike3Points) {
			PlayerPrefs.SetInt("selectedBike",3);
			bike1Selected.SetActive(false);
			bike2Selected.SetActive(false);
			bike3Selected.SetActive(true);
		}
	}



}
