﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LevelSelectMenu : MonoBehaviour {

	public GameObject[] locks;
	public RectTransform levelSelectPanel;
	public int myChapter;


	//private variables
	private int canLoadLevel;
	// Use this for initialization
	void Start () {

		if(myChapter == 1) {
			checkPlayerProgressChapter1();
		}

		if(myChapter == 2) {
			checkPlayerProgressChapter2();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void checkPlayerProgressChapter1() {

		Vector3 pos = levelSelectPanel.transform.localPosition;
		string levelName;
		int j = 0;
		canLoadLevel = 1;


		for(int i = 1; i <= 18; i++) { //we start from number 2 because level 1 is already open
			levelName = "level" + i;
			if(PlayerPrefs.HasKey(levelName)) {
				canLoadLevel = canLoadLevel + 1;
				locks[j].SetActive(false);
				j++;

				//position the level button to the completed level
				switch (i) {
				case 1:
					pos.x = 2165.743f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 2:
					pos.x = 1862.36f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 3:
					pos.x = 1555.818f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 4:
					pos.x = 1258.138f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 5:
					pos.x = 963.5608f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 6:
					pos.x = 659.6774f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 7:
					pos.x = 361.3545f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 8:
					pos.x = 63.99921f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 9:
					pos.x = -240.9938f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 10:
					pos.x = -547.3761f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 11:
					pos.x = -838.8557f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 12:
					pos.x = -1137.261f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 13:
					pos.x = -1431.942f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 14:
					pos.x = -1731.989f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 15:
					pos.x = -2031.441f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 16:
					pos.x = -2337.2f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 17:
					pos.x = -2642.581f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 18:
					pos.x = -2642.581f;
					levelSelectPanel.transform.localPosition = pos;
					break;

				}
			}
		}
	}

	void checkPlayerProgressChapter2() {
		
		Vector3 pos = levelSelectPanel.transform.localPosition;
		string levelName;
		int j = 0;
		canLoadLevel = 18;
		
		
		for(int i = 19; i <= 29; i++) { 
			levelName = "level" + (i-1);
			if(PlayerPrefs.HasKey(levelName)) {
			
				canLoadLevel = canLoadLevel + 1;
				locks[j].SetActive(false);
				j++;
				
				//position the level button to the completed level
				switch (i) {
				case 19:
					pos.x = 1379.146f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 20:
					pos.x = 1102.98f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 21:
					pos.x = 783.2119f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 22:
					pos.x = 479.6494f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 23:
					pos.x = 179.237f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 24:
					pos.x = -122.4254f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 25:
					pos.x = -440.7989f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 26:
					pos.x = -733.9576f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 27:
					pos.x = -1043.103f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 28:
					pos.x = -1339.368f;
					levelSelectPanel.transform.localPosition = pos;
					break;
				case 29:
					pos.x = -1608.613f;
					levelSelectPanel.transform.localPosition = pos;
					break;
					
				}
			}
		}
	}

	public void selectLevel(int levelNo) {

		Debug.Log(levelNo);
		string levelName = "level";

		if(levelNo <= canLoadLevel) {
			levelName = levelName + levelNo;
			Debug.Log(levelName);
			Application.LoadLevel(levelName);
		}
	}
}
