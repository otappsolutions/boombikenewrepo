﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {

	public GameObject soundOnOff;
	public GameObject play;
	public GameObject bikeSelect;
	public GameObject back;
	public GameObject bikeSelectMenu;
	public GameObject totalPoints;
	public ScoreKeeperScript scoreKeeper;
	public Text TotalPointsText;
	public Toggle soundOnOffToggle;
	public GameObject chapterSelect;
	public GameObject levelSelectChapter1;
	public GameObject levelSelectChapter2;
	private int soundOnOrOff = 1; //on is the default
	

	// Use this for initialization
	void Start () {



		if(PlayerPrefs.HasKey("soundOnOff")) {

			if(PlayerPrefs.GetInt("soundOnOff") == 1) {
				soundOnOffToggle.isOn = true;
				AudioListener.volume = 1;
			}
			else {
				soundOnOffToggle.isOn = false;
				AudioListener.volume = 0;
			}

		}
		else { //means this is first time we running the game
			PlayerPrefs.SetInt("soundOnOff",soundOnOrOff);
			soundOnOffToggle.isOn = true; //set the default initial value which is on
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void bikeSelectAction () {

		GetComponent<AudioSource>().Play();
		//bike select menu
		bikeSelectMenu.SetActive(true);
		back.SetActive(true);
		totalPoints.SetActive(true);

		TotalPointsText.text = "Total Points: " + scoreKeeper.getTotalScore();

		//main menu
		play.SetActive(false);
		bikeSelect.SetActive(false);
		soundOnOff.SetActive(false);
	}

	public void goToMainMenu () {

		GetComponent<AudioSource>().Play();
		//bike select menu
		bikeSelectMenu.SetActive(false);
		back.SetActive(false);
		totalPoints.SetActive(false);

		//level and chapter select
		chapterSelect.SetActive(false);
		levelSelectChapter1.SetActive(false);
		levelSelectChapter2.SetActive(false);

		//main menu
		play.SetActive(true);
		bikeSelect.SetActive(true);
		soundOnOff.SetActive(true);

	}

	public void soundOnOffAction () {

		GetComponent<AudioSource>().Play();
		if(soundOnOffToggle.isOn) {

			soundOnOrOff = 1;
			PlayerPrefs.SetInt("soundOnOff",soundOnOrOff);
			AudioListener.volume = 1;
		}
		else {
		
			soundOnOrOff = 0;
			PlayerPrefs.SetInt("soundOnOff",soundOnOrOff);
			AudioListener.volume = 0;
		}
	}

	public void playAction() {

		GetComponent<AudioSource>().Play();
		//bike select menu
		bikeSelectMenu.SetActive(false);
		back.SetActive(true);
		totalPoints.SetActive(false);
		
		//level and chapter select
		chapterSelect.SetActive(true);
		levelSelectChapter1.SetActive(false);
		
		//main menu
		play.SetActive(false);
		bikeSelect.SetActive(false);
		soundOnOff.SetActive(false);

	}

	public void chapterSelectAction(int chapterNo) {

		GetComponent<AudioSource>().Play();
		//hide chapters menu
		chapterSelect.SetActive(false);

		if(chapterNo == 1) {
				levelSelectChapter1.SetActive(true);
		}

		if(chapterNo == 2) {
			levelSelectChapter2.SetActive(true);
		}
	}


}
