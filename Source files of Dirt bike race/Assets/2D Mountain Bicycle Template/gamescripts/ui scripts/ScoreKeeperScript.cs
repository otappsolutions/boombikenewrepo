﻿using UnityEngine;
using System.Collections;

public class ScoreKeeperScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void saveScore(int newScore) {
	
		string scoreID = "score" + Application.loadedLevelName;
		int savedScore = 0;
		//check if we have score already and if current score is higher then save else do not save
		if(PlayerPrefs.HasKey(scoreID)) {
			savedScore = PlayerPrefs.GetInt(scoreID);

			if(savedScore < newScore) {
				PlayerPrefs.SetInt(scoreID,newScore);
				updateTotalScore(newScore,savedScore);
			}
		}
		else {
			PlayerPrefs.SetInt(scoreID,newScore);
			updateTotalScore(newScore,0);

		}
	}

	public int getScore() {

		string scoreID = "score" + Application.loadedLevelName;

		if(PlayerPrefs.HasKey(scoreID)) {
			return PlayerPrefs.GetInt(scoreID);
		}

		//if we reach here then there is no score saved already
		return 0;
	}

	void updateTotalScore(int newScore, int oldScore) {

		int totalScore = 0;

		if(PlayerPrefs.HasKey("totalScore")) {

			totalScore = PlayerPrefs.GetInt("totalScore");
			totalScore = totalScore - oldScore;
			totalScore = totalScore + newScore;
			PlayerPrefs.SetInt("totalScore",totalScore);
		}
		else {
			totalScore = totalScore - oldScore;
			totalScore = totalScore + newScore;

			PlayerPrefs.SetInt("totalScore",totalScore);
		}
	}

	public int getTotalScore () {
	
		if(PlayerPrefs.HasKey("totalScore")) {

			return PlayerPrefs.GetInt("totalScore");
		}

		return 0; //if we reach here means we do not have total score so return 0
	}
}
