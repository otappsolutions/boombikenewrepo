﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class ControlButtonScript : UIBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler {

	public enum myAction { leanBack,LeanForward,Brake,Accelerate};
	
	public myAction action;
	
	public void OnPointerDown( PointerEventData eventData ) {

		switch(action) {

		case myAction.leanBack:
			GameObject.FindWithTag("bikebody").GetComponent<BikeController>().doLeanBack();
			break;

		case myAction.LeanForward:
			GameObject.FindWithTag("bikebody").GetComponent<BikeController>().doLeanForward();
			break;

		case myAction.Brake:
			GameObject.FindWithTag("bikebody").GetComponent<BikeController>().doBrake();
			break;

		case myAction.Accelerate:
			GameObject.FindWithTag("bikebody").GetComponent<BikeController>().doAccelerate();
			break;
	}
	}

	
	public void OnPointerUp( PointerEventData eventData ) {

		switch(action) {
			
		case myAction.leanBack:
			GameObject.FindWithTag("bikebody").GetComponent<BikeController>().stopLeanBack();
			break;
			
		case myAction.LeanForward:
			GameObject.FindWithTag("bikebody").GetComponent<BikeController>().stopLeanForward();
			break;
			
		case myAction.Brake:
			GameObject.FindWithTag("bikebody").GetComponent<BikeController>().stopBrake();
			break;
			
		case myAction.Accelerate:
			GameObject.FindWithTag("bikebody").GetComponent<BikeController>().stopAccelerate();
			break;
		}
	}
	
	
	public void OnPointerExit( PointerEventData eventData ) {

	}
}