﻿using UnityEngine;
using System.Collections;

public class SlowOrSpeedTime : MonoBehaviour {

	public bool slowTime;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
	
		if(other.gameObject.tag == "player") {
			slowOrSpeedTime();
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
		
		if(other.gameObject.tag == "player") {

			slowOrSpeedTime();
		}
	}

	void slowOrSpeedTime() {

		if(slowTime) {
			Time.timeScale = 0.3f;
		}
		else {
			Time.timeScale = 1.0f;
		}
	}
}
