﻿using UnityEngine;
using System.Collections;

public class ActivateDeactivateObject : MonoBehaviour {

	public GameObject obj;
	public bool activate = false;
	private bool triggered = false; 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		
		if(other.gameObject.tag == "tire" && !triggered) {
			doAction();
			triggered = true;
		}
	}

	void OnCollisionEnter2D(Collision2D other) {
		
		if(other.gameObject.tag == "tire" && !triggered) {
			doAction();
			triggered = true;
		}
	}

	void doAction() {

			
			if(activate) {
				obj.SetActive(true);
			}
			else {
				obj.SetActive(false);
			}
		}
	

}
