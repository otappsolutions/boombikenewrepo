﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SceneConrtoller : MonoBehaviour {

	public Text guiTimeText;
	public Image menuBackground;
	public Button youtube;
	public Button mainmenu;
	public Button replay;
	public Text pauseText;
	public Text gameoverText;
	public Image pauseBackground;
	public Toggle pauseResume;
	public Text finalScore;
	public Text score;
	public Text finalTime;
	public Text theTime;
	public Text bestScore;
	public Text theBestScore;
	public int maximumScore = 1200;
	public ScoreKeeperScript scoreKeeper;
	public Button next;
	public int maximumLevels = 18;//maximum levels we have
	//private variables
	private float startTime;
	private bool gamePaused = false;
	private bool gameOver = false;
	private int endScore;
	private bool beginTimer = false;

	// Use this for initialization
	void Start () {

		//start everyolay recording when level starts
		//Everyplay.StopRecording();
		//Everyplay.StartRecording();
	}

	void Awake() {

	}

	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate () {

		updateGUITime();
	}

	void updateGUITime() {
	
		if(gameOver || !beginTimer) 
			return;

		float guiTime = Time.time - startTime;
		int minutes;
		int seconds;
		int fraction;

		minutes = (int) guiTime / 60;
		seconds = (int) guiTime % 60;
		fraction = (int) (guiTime * 100) % 100;

		guiTimeText.text = string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, fraction);

		calculateScore(minutes,seconds);
	}

	//pause or resume the game
	public void pauseResumeGame () {
		playClickSound();
		Debug.Log("paused");
		gamePaused = !gamePaused;

		if(gamePaused) {
			showHidePausedMenu();
			Time.timeScale = 0.0f;
		}
		else {
			Time.timeScale = 1.0f;
			showHidePausedMenu();
		}

	}

	void showHidePausedMenu() {
		playClickSound();
		if(gamePaused) {

			pauseBackground.gameObject.SetActive(true);
			replay.gameObject.SetActive(true);
			mainmenu.gameObject.SetActive(true);
			pauseText.gameObject.SetActive(true);


		}

		else {

			pauseBackground.gameObject.SetActive(false);
			replay.gameObject.SetActive(false);
			mainmenu.gameObject.SetActive(false);
			pauseText.gameObject.SetActive(false);
		}
	}

	public void repeat() {
		Time.timeScale = 1.0f;
		playClickSound();
		Application.LoadLevel(Application.loadedLevelName);
	}

	public void goToMainMenu () {

		Time.timeScale = 1.0f;
		Application.LoadLevel("mainmenu");
	}

	

	public void showEveryplay() {
	
		//Everyplay.ShowSharingModal();
	}

	void playClickSound() {
		GetComponent<AudioSource>().Play();
	}


	public void showGameOver(bool fallen) {

		//show an add first :)

		//do game over stuff next
		gameOver = true;
		pauseResume.gameObject.SetActive(false);
		menuBackground.gameObject.SetActive(true);
		replay.gameObject.SetActive(true);
		mainmenu.gameObject.SetActive(true);
		youtube.gameObject.SetActive(true);
		gameoverText.gameObject.SetActive(true);
		finalScore.gameObject.SetActive(true);
		finalTime.gameObject.SetActive(true);
		bestScore.gameObject.SetActive(true);

		//show final time time
		theTime.text = guiTimeText.text;

		//show final score
		if(!fallen) { //score should be zero

			//if this is not the final level, then show next button
			if(Application.loadedLevel != maximumLevels) {
				next.gameObject.SetActive(true); //only show next button if player has finished the level
			}

			score.text = "" + endScore;
			//save final score
			scoreKeeper.saveScore(endScore);
		}

		//get best score for this level
		theBestScore.text = "" + scoreKeeper.getScore();
	}

	void calculateScore (int minutes, int seconds) {

		//the score system, each level has a score of 1200 maximum and minimum of 100
		//if player finish each 10 seconds reduce the score by 100

		int totalTime = (minutes * 60) + seconds;

		if(totalTime < 20) {
			endScore = maximumScore;
		}
		if(totalTime > 20) {
			endScore = maximumScore - 100;
		}
		if(totalTime > 40) {
			endScore = maximumScore - 200;
		}
		if(totalTime > 50) {
			endScore = maximumScore - 300;
		}
		if(totalTime > 60) {
			endScore = maximumScore - 400;
		}
		if(totalTime > 70) {
			endScore = maximumScore - 500;
		}
		if(totalTime > 90) {
			endScore = maximumScore - 600;
		}
		if(totalTime > 100) {
			endScore = maximumScore - 700;
		}
		if(totalTime > 110) {
			endScore = maximumScore - 800;
		}
		
	}

	public void startTimer () {

		if(beginTimer)
			return; //call this function once
		beginTimer = true;
		//get a hold of start time
		startTime = Time.time;
	}

	public void stopTimer() {
		beginTimer = false;
	}

	public void nextLevel() {

		Application.LoadLevel(Application.loadedLevel + 1);
	}


}
